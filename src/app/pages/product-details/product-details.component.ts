import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Product } from "src/app/interfaces/product";
import { ProductService } from "src/app/services/product.service";

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent {
    productId = -1;
    product: Product | undefined;
    applyForm = new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email])
    });
    submitted = false;

    constructor(
        private route: ActivatedRoute,
        private productService: ProductService
    ) {
        this.productId = parseInt(this.route.snapshot.params['id'], 10);
        this.productService.getProductById(this.productId).then(product => {
            this.product = product;
        });
    }

    get firstName(): any {
        return this.applyForm.get('firstName');
    }

    get lastName(): any {
        return this.applyForm.get('lastName');
    }

    get email(): any {
        return this.applyForm.get('email');
    }
    submitForm() {
        this.submitted = true;
        if (this.applyForm.valid) {
            console.log(this.applyForm.value);
        }
    }
}