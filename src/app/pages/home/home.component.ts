import { Component } from "@angular/core";
import { Product } from "src/app/interfaces/product";
import { ProductService } from "src/app/services/product.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {
    productList!: Product[];
    filteredProductList: Product[] | undefined;

    constructor(private productService: ProductService) { 
        this.productService.getAllProducts().then((productList: Product[]) => {
            this.productList = productList;
            this.filteredProductList = productList;
        });
    }

    filterResults(name: string) {
        if (!name) {
            this.filteredProductList = this.productList;
        }

        this.filteredProductList = this.productList.filter(
            product => product?.name.toLowerCase().includes(name.toLowerCase())
        );
    }
}